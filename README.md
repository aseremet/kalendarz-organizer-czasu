# Kalendarz - organizer czasu

Projekt ma na celu stworzenie aplikacji internetowej, dzięki której użytkownik będzie mógł efektywnie zarządzać swoim czasem. 
Aplikacja będzie udostępniać funkcjonalności takie jak:
 - rejestracja i logowanie użytkownika, 
 - planer dzienny, miesięczny i roczny (osobne widoki dla każdego planera z możliwością wypisania rzeczy, które chce się zrealizować w danym czasie),
 - oznaczenie poziomu wykonania zadania (mierzalność poziomu wykonania zadania zależy od jego typu - aby zachować jednolity sposób mierzenia dla każdego zadania, aplikacja będzie umożliwiała ręczne ustawienie poziomu),
 - wybór przykładowych zadań z listy i wygodne oznaczenie ich w kalendarzu,
 - wybór codziennych zadań i monitorowanie regularności (zadania będzie można ręcznie dodać do kalendarza, przesuwając box z zadaniem na wybrany dzień w kalendarzu na zasadzie Drag and Drop),
 - możliwość wyboru zadań na wybrane przez użytkownika dni tygodnia,
 - możliwość tagowania aktywności i filtrowania ich,
 - oznaczenie polskich świąt narodowych,
 - lista rzeczy do zrobienia w ciągu dnia, miesiąca i roku (zadania będą należeć do osobnej kategorii, w których nie będzie ustawienia poziomu wykonania zadania, elementy będą w formie check listy - jeżeli użytkownik wykona daną czynność, może odznaczyć zadanie jako wykonane).

Dodatkowo aplikacja będzie wykorzystywała serwer publiczny.
