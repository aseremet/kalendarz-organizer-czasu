import React, { Component, useEffect, useState } from 'react';
import { Icon, Header, Form, Segment, Message, TextArea, Button } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import {
  auth,
  registerWithEmailAndPassword,
  signInWithGoogle,
} from "./firebase";
import "./MainHeader.css";

function MainHeader({name, logout}) {
  const [user, loading, error] = useAuthState(auth);
  const [toDoMonth, setToDoMonth] = useState([]);
  const history = useNavigate();
  useEffect(() => {
    if (loading) return;
  }, [user, loading]);
  return(
    <div className='mainHeader'>
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <Segment textAlign='center' attached="top">{name}</Segment>
                <Button color='teal' size='small' onClick={logout} attached="bottom">
                    Wyloguj się
                </Button>
                </div>
                <div class="col-md-8"></div>
            <div class="col-md-2 logo-column"><img src="/logo.png"/></div>
            </div>
        </div>
    </div>
  )
}

export default MainHeader;
