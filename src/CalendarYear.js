import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import { Helmet } from 'react-helmet';
import { Modal, Button, Form } from 'semantic-ui-react';
import Calendar from 'short-react-calendar';
import { query, collection, getDocs, where, addDoc, onSnapshot } from "firebase/firestore";
import "./CalendarYear.css";
import { auth, db, logout } from "./firebase";

const months = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień'];

function CalendarYear() {
  const [user, loading, error] = useAuthState(auth);
  const [name, setName] = useState("");
  const [toDoMonth, setToDoMonth] = useState([]);
  //const [toDoToday, setToDoToday] = useState([]);
  const [title, setTitle] = useState("");
  const [tag, setTag] = useState("");
  const [open, setOpen] = useState(false)
  const navigate = useNavigate();

  
  const q = user && query(collection(db, "toDoMonth"), where("uid", "==", user.uid));
  const unsubscribe = user ? onSnapshot(q, (querySnapshot) => {
    const arrTasks = [];
    querySnapshot.forEach((doc) => {
        arrTasks.push(doc.data());
    });
    setToDoMonth(arrTasks)
  }) : () => {};


  const fetchUserName = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();
      setName(data.name);
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching user data");
    }
  };
  const fetchToDoMonth = async () => {
    try {
        const q = query(collection(db, "toDoMonth"), where("uid", "==", user.uid));
      const res = await getDocs(q);
      res.forEach((task) => {
        setToDoMonth(toDoMonth => [...toDoMonth, task.data()]);
      })
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching to do list");
    }
  };
  const addTask = async (month) => {
    try {
        const docRef = await addDoc(collection(db, "toDoMonth"), {
            title: title,
            tag: tag,
            uid: user.uid,
            month: month,
        });
        setTitle('')
        setTag('')
    } catch (err) {
        console.error(err);
        alert("An error occured while adding task");
    }
  }
  /*const fetchToDoToday = async () => {
    try {
        const date = new Date(new Date().setHours(0,0,0,0));
        const q = query(collection(db, "toDoToday"), where("uid", "==", user.uid), where("date", "==", date));
        const res = await getDocs(q);
      res.forEach((task) => {
          console.log(task.id, ' => ', task.data())
        setToDoToday(toDoToday => [...toDoToday, task.data()]);
      })
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching to do list");
    }
  };*/
  useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/");
    fetchUserName();
    fetchToDoMonth();
    //fetchToDoToday();
  }, [user, loading]);
  return (
    <div className="yearContent">
        <Helmet title="Kalendarz roczny" />
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-1 sidebar-nav-menu">
                    <i type="submit" onClick={logout} class="bi bi-power logout-icon"></i>
                </div>
                <div class="col-md-11 content-dashboard">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Mini Kalendarz</h2>
                        </div>
                        <div class="col-md-6">
                        <Calendar
                            oneWeekCalendar={true}
                        />
                        </div>
                    </div>
                    <div class="row calendar-year-view">
                        <div class="col-md-12">
                            {months.map((month, i) =>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><Link to={month}>{month.toUpperCase()}</Link></h5>
                                    <Modal
                                        onClose={() => setOpen(false)}
                                        onOpen={() => setOpen(month)}
                                        open={month==open}
                                        trigger={<i onClick={()=>{}} class="bi bi-plus-square"></i>}
                                        className="modalStyle"
                                        >
                                        <Modal.Header>Dodaj zadanie</Modal.Header>
                                        <Modal.Content>
                                            <Modal.Description>
                                            <Form size='large'>
                                                    <Form.Input
                                                        fluid
                                                        placeholder='Tytuł'
                                                        value={title}
                                                        onChange={(e) => setTitle(e.target.value)}/>
                                                    <Form.Input
                                                        fluid
                                                        placeholder='Tag'
                                                        value={tag}
                                                        onChange={(e) => setTag(e.target.value)}/>
                                            </Form>
                                            </Modal.Description>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button color='red' onClick={() => setOpen(false)}>
                                            Anuluj
                                            </Button>
                                            <Button
                                            content="Dodaj"
                                            labelPosition='right'
                                            icon='checkmark'
                                            onClick={() => {
                                                addTask(month)
                                                setOpen(false)
                                            }}
                                            positive
                                            />
                                        </Modal.Actions>
                                    </Modal>
                                    <h6 class="card-subtitle mb-2 text-muted">2022</h6>
                                    <ul class="list-group">
                                        {toDoMonth.map(task => (
                                            task.month == month &&
                                            <li class="list-group-item">{task.title.toUpperCase()}</li>
                                            ))}
                                    </ul>
                                </div>
                            </div>)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);}

export default CalendarYear;

