import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
//import Header from './Header';
//import Footer from './Footer';
//import MainMenu from './MainMenu';
import CalendarYear from './CalendarYear';
import CalendarMonth from './CalendarMonth';
import CalendarDay from './CalendarDay';
import LoginPage from './LoginPage';
//import NoMatch from './NoMatch';
import './App.css';
import Register from './Register';

class App extends Component {
    render(){
        return(
          <div>
              <Routes>
                <Route path="/" element={<LoginPage />} />
                <Route path="/rejestracja" element={<Register />} />
                <Route path="/dashboard" element={<CalendarYear />} />
                <Route path="/dashboard/:month" element={<CalendarMonth />} />
                <Route path="/dashboard/:month/:day" element={<CalendarDay />} />

                {/*<Route path="/rok" component={CalendarYear} />*/}
              </Routes>
          </div>
        )
    }
}

export default App;
