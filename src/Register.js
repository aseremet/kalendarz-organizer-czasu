import React, { Component, useEffect, useState } from 'react';
import { Grid, Icon, Header, Form, Segment, Message, TextArea, Button } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import {
  auth,
  registerWithEmailAndPassword,
  signInWithGoogle,
} from "./firebase";
import "./Register.css";



function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();
    const register = () => {
      if (!name) alert("Please enter name");
      registerWithEmailAndPassword(name, email, password);
    };
    console.log(navigate)
    useEffect(() => {
      if (loading) return;
      if (user) navigate("/dashboard");
    }, [user, loading]);
    return(
        <div className='loginPage'>
            <Helmet title="Zarejestruj się" />
            <div class="container">
                <div class="row content-row">
                    <div class="col-md-6"><img src="/logo.png"/></div>
                    <div class="col-md-6">
                        <h1 id="h1">Zarejestruj się</h1>
                        <Grid textAlign='center' verticalAlign='middle' padded="padded">
                            <Grid.Column
                                style={{
                                    maxWidth: 450
                                }}>
                                <Form size='large'>
                                    <Segment stacked="stacked">
                                        <Form.Input
                                            fluid="fluid"
                                            icon='user'
                                            iconPosition='left'
                                            placeholder='E-mail'
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}/>
                                        <Form.Input
                                            fluid="fluid"
                                            icon='user'
                                            iconPosition='left'
                                            placeholder='Imie i nazwisko'
                                            value={name}
                                            onChange={(e) => setName(e.target.value)}/>
                                        <Form.Input
                                            fluid="fluid"
                                            icon='lock'
                                            iconPosition='left'
                                            placeholder='Hasło'
                                            type='password'
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}/>

                                        <Button color='Gray' fluid="fluid" size='large' onClick={register}>
                                            Zerejestruj
                                        </Button>
                                        <br/>
                                        <Button color='Gray' fluid="fluid" size='large' onClick={signInWithGoogle}>
                                            <Icon name='google'/>
                                            Zarejestruj przez Google
                                        </Button>
                                    </Segment>
                                </Form>
                                <Message>
                                    Masz już konto?
                                    <Link to="/"> Zaloguj się</Link>
                                </Message>
                            </Grid.Column>
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register;
