import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import { Helmet } from 'react-helmet';
import { Card, Button } from 'semantic-ui-react';
import { query, collection, getDocs, where } from "firebase/firestore";
import FullCalendar from '@fullcalendar/react' // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid' // a plugin!
import "./CalendarDay.css";
import { auth, db, logout } from "./firebase";
import MainHeader from './MainHeader';

const months = [];


function CalendarYear() {
  const [user, loading, error] = useAuthState(auth);
  const [name, setName] = useState("");
  const navigate = useNavigate();
  const fetchUserName = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();
      setName(data.name);
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching user data");
    }
  };
  useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/");
    fetchUserName();
  }, [user, loading]);


  return (
    <div className="monthContent">
    <Helmet title="Kalendarz roczny" />
    <div class="container-fluid">
            <div class="row">
                <div class="col-md-1 sidebar-nav-menu">
                    <i class="bi bi-arrow-bar-left logout-icon" onClick={() => navigate(-1)}></i>
                    <i class="bi bi-file-earmark-plus logout-icon"></i>
                    <a href="#"><i class="bi bi-cloud-upload logout-icon"></i></a>
                    <i type="submit" onClick={logout} class="bi bi-power logout-icon"></i>
                </div>
                <div class="col-md-11 month-board">
                    <div class="row">
                        <div class="col-md-4">
                          Drag and drop
                        </div>
                        <div class="col-md-8">
                        <h1>Lista zadań na dziś</h1>
                            <ul class="list-group day-task-list">
                                <li class="list-group-item">Zadanie I</li>
                                <li class="list-group-item">Zadanie II</li>
                                <li class="list-group-item">Zadanie III</li>
                                <li class="list-group-item">Zadanie IV</li>
                                <li class="list-group-item">Zadanie V</li>
                            </ul>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12"></div>
                    </div>
                </div>
            </div>

            
            </div>
</div>
  );
}
export default CalendarYear;



