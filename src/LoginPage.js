import React, { Component, useEffect, useState } from 'react';
import { Grid, Icon, Header, Form, Segment, Message, TextArea, Button } from 'semantic-ui-react';
import { Helmet } from 'react-helmet';
import { Link, useNavigate } from "react-router-dom";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, logInWithEmailAndPassword, signInWithGoogle } from "./firebase";

import './LoginPage.css';

function LoginPage() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();
    useEffect(() => {
      if (loading) {
        // maybe trigger a loading screen
        return;
      }
      if (user) navigate("/dashboard");
    }, [user, loading]);
    return (
        <div className='loginPage'>
            <Helmet title="Zaloguj się" />
            <div class="container">
                <div class="row content-row">
                    <div class="col-md-6"><img src="/logo.png"/></div>
                    <div class="col-md-6">
                        <h1 id="h1">Zoptymalizuj swój czas</h1>
                        <Grid textAlign='center' verticalAlign='middle' padded="padded">
                            <Grid.Column
                                style={{
                                    maxWidth: 450
                                }}>
                                <Form size='large'>
                                    <Segment stacked="stacked">
                                        <Form.Input
                                            fluid="fluid"
                                            icon='user'
                                            iconPosition='left'
                                            placeholder='E-mail'
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}/>
                                        <Form.Input
                                            fluid="fluid"
                                            icon='lock'
                                            iconPosition='left'
                                            placeholder='Hasło'
                                            type='password'
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}/>

                                        <Button
                                            color='Gray'
                                            fluid="fluid"
                                            size='large'
                                            onClick={() => logInWithEmailAndPassword(email, password)}>
                                            Zaloguj
                                        </Button>
                                        <br/>
                                        <Button color='Gray' fluid="fluid" size='large' onClick={signInWithGoogle}>
                                            <Icon name='google'/>
                                            Zaloguj przez Google
                                        </Button>
                                    </Segment>
                                </Form>
                                <Message>
                                    Jesteś nowy?
                                    <Link to="/rejestracja">Zarejestruj się</Link>
                                </Message>
                            </Grid.Column>
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginPage;
