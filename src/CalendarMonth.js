import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate, 
  useRouteMatch,
  useParams  } from "react-router-dom";
import { Helmet } from 'react-helmet';
import { Card, Button } from 'semantic-ui-react';
import { query, collection, getDocs, where } from "firebase/firestore";
import FullCalendar from '@fullcalendar/react' // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid' // a plugin!
import "./CalendarYear.css";
import { auth, db, logout } from "./firebase";
import MainHeader from './MainHeader';

const months = ['', 'styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień'];


function CalendarYear() {
  const [user, loading, error] = useAuthState(auth);
  const [name, setName] = useState("");
  const [toDoMonth, setToDoMonth] = useState([]);
  const navigate = useNavigate();
  const fetchUserName = async () => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();
      setName(data.name);
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching user data");
    }
  };
  const fetchToDoMonth = async () => {
    try {
      const q = query(collection(db, "toDoMonth"), where("uid", "==", user.uid));
      const res = await getDocs(q);
      res.forEach((task) => {
        setToDoMonth(toDoMonth => [...toDoMonth, task.data()]);
      })
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching to do list");
    }
  };
  useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/");
    fetchUserName();
    fetchToDoMonth();
  }, [user, loading]);

  let { month } = useParams();

  return (
    <div className="monthContent">
    <Helmet title="Kalendarz roczny" />
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-1 sidebar-nav-menu">
              <i class="bi bi-arrow-bar-left logout-icon" onClick={() => navigate(-1)}></i>
              <i class="bi bi-file-earmark-plus logout-icon"></i>
              <a href="#"><i class="bi bi-cloud-upload logout-icon"></i></a>
              <i type="submit" onClick={logout} class="bi bi-power logout-icon"></i>
              <Link to="1">
                  <button>klik</button>
              </Link>
          </div>
          <div class="col-md-11 month-board">
              <div class="row">
                  <div class="col-md-3">
                  {toDoMonth.map(task => (
                    task.month === month && <li class="list-group-item">{task.title.toUpperCase()}</li>
                    ))}
                  </div>
                  <div class="col-md-9">
                      <FullCalendar
                          plugins={[dayGridPlugin]}
                          initialView="dayGridMonth"
                          weekends={false}
                          events={[
                              {
                                  title: 'event 1',
                                  date: '2022-02-01'
                              }, {
                                  title: 'event 2',
                                  date: '2022-02-02'
                              }
                          ]}/>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12"></div>
              </div>
              </div>
      </div>
      </div> </div>
  );
}
export default CalendarYear;



